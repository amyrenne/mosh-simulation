'''
Created on May 18, 2020

@author: amyrenne
'''
'''
Created on Apr 30, 2020
Last Update: May 6, 2020
@author: Amy Renne
'''

import turtle
import random
import math

'''Establishes collision counters'''
blu_col = 0
gn_col = 0
col = 0

wn = turtle.Screen()
wn.bgcolor("black")
wn.title("Mosh Simulator")
wn.tracer(8)

'''Defines list of blues starting on the left wall'''
blues = []
N = 20

for i in range(N):
    blues.append(turtle.Turtle())

for blue in blues:
    blue.shape("circle")
    blue.color("blue")
    #
    blue.penup()
    blue.speed(0)
    r = 290
    theta = random.randint(0,180)
    y_blue = r*math.sin(theta)
    x_blue = r*math.cos(theta)
    blue.goto(x_blue, y_blue)
    blue.mass = random.randint(1,3)
    #blue.mass = 1
    
'''Defines list of greens starting on the right wall'''
greens = []
M = 20
 
for i in range(M):
    greens.append(turtle.Turtle())
 
for green in greens:
    green.shape("circle")
    green.color("green")
    green.penup()
    green.speed(0)
    r = 290
    theta = random.randint(0,180)
    y_green = r*math.sin(theta)
    x_green = r*math.cos(theta)
    green.goto(x_green, y_green)
     
 
'''Creates a function that checks if two moshers collided'''
def isCollision(A1, A2):
    r = math.sqrt(math.pow(A1.xcor() - A2.xcor(), 2) + math.pow(A1.ycor() - A2.ycor(), 2))
    if r < 20:
        return True
     
'''Creates a function to show the spread of a mosh population'''
def stDev(A1):
    sum_r = 0
    for a in A1[1:len(A1)]:
        i = A1.index(a)
        for b in A1[0:i]:
            j = A1.index(b)
            dist = math.sqrt( math.pow(A1[i].xcor() - 
                                       A1[j].xcor(), 2) + math.pow(A1[i].ycor() - A1[j].ycor(), 2 ) )
            #dist = math.sqrt( math.pow( math.pow(a.xcor() - b.xcor(), 2) + math.pow(a.ycor() - b.ycor(), 2)) )
            sum_r += dist
    sd = math.sqrt( math.pow(sum_r, 2)/(len(A1)) )
    print(sd)
 
'''Creates a function that updates moshers coordinates if two moshers collided'''
def doSomething(A1,A2): 
    theta_1 = random.randint(-15,15)
    theta_2 = random.randint(-15,15)
    r_1 = random.randint(250, 295)
    r_2 = random.randint(250, 295)
    A1.setx(r_1*math.cos(theta_1))
    A1.sety(r_1*math.sin(theta_1))
    A2.setx(r_2*math.cos(theta_2))
    A2.sety(r_2*math.sin(theta_2))
    
'''Animate'''
while True:
    wn.update()
    for blue in blues:
        #assume wall of death, everyone accelerating in x-dir, random motion in y
        theta += random.randint(15,30)
        r = random.randint (250,290)
        blue.setx(r*math.cos(theta))
        blue.sety(r*math.sin(theta))
            
            
    for green in greens:
    #assume wall of death, everyone accelerating in -x-dir, random motion in y
        theta += random.randint(15,30)
        r = random.randint (250,290)
        green.setx(r*math.cos(theta))
        green.sety(r*math.sin(theta))
#         
#                
#                 
#     'CHECKS FOR IN GROUP COLLISIONS'   
#     for blue in blues:
#         i = blues.index(blue)
#         for blue in blues[0:i]:
#             j = blues.index(blue)
#             if i != j and isCollision(blue, blue):
#                 blu_col += 1
#                 excuseMe(blue, blue)
#                 #print("blues = " + str(blu_col)) 
#                   
#     for green in greens:
#         i = greens.index(green)
#         for green in greens[0:i]:
#             j = greens.index(green)
#             gn_col += 1
#             if i != j and isCollision(green, green):
#                 excuseMe(green, green)
#                 #print("greens = " + str(gn_col))
#      
    '''CHECKS FOR COLLISIONS BETWEEN GROUPS'''
    for green in greens: 
        for blue in blues:
            if isCollision(blue, green):
                doSomething(blue,green)
                print("collision")

      
                  
turtle.mainloop()