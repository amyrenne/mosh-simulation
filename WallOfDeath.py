'''
Created on Apr 30, 2020
Last Update: May 6, 2020
@author: Amy Renne
'''

import turtle
import random
import math

'''Establishes collision counters'''
blu_col = 0
gn_col = 0
col = 0

wn = turtle.Screen()
wn.bgcolor("black")
wn.title("Mosh Simulator")
wn.tracer(0)

'''Defines list of blues starting on the left wall'''
blues = []
N = 30

for i in range(N):
    blues.append(turtle.Turtle())

for blue in blues:
    blue.shape("circle")
    blue.color("blue")
    blue.penup()
    blue.speed(0)
    y_blue = random.randint(-290,290)
    blue.goto(-290, y_blue)
    blue.mass = random.randint(1,3)
    #blue.mass = 1
    blue.dx = random.randint(0,3)
    blue.dy = 0.5

'''Defines list of greens starting on the right wall'''
greens = []
M = 30

for i in range(M):
    greens.append(turtle.Turtle())

for green in greens:
    green.shape("circle")
    green.color("green")
    green.penup()
    green.speed(0)
    y_green = random.randint(-290,290)
    green.goto(290, y_green) 
    green.mass = random.randint(1,3) 
    #green.mass = 5
    green.dx = random.randint(0,3)
    green.dy = 0
    
'''Defines a wildcard mosher'''
wilds = []
N = 10
for i in range(N):
    wilds.append(turtle.Turtle())

for wild in wilds:
    wild.shape("circle")
    wild.color("orange")
    #wild.penup()
    wild.speed(5)
    x_wild = random.randint(-290,290)
    y_wild = random.randint(-290,290)
    wild.goto(0,0)
    wild.mass = random.randint(1,3)
    #blue.mass = 1
    wild.dx = random.randint(0,3)
    wild.dy = 0.5

'''Creates a function that checks if two moshers collided'''
def isCollision(A1, A2):
    r = math.sqrt(math.pow(A1.xcor() - A2.xcor(), 2) + math.pow(A1.ycor() - A2.ycor(), 2))
    if r < 20:
        return True
    
'''Creates a function to show the spread of a mosh population'''
def stDev(A1):
    sum_r = 0
    for a in A1[1:len(A1)]:
        i = A1.index(a)
        for b in A1[0:i]:
            j = A1.index(b)
            dist = math.sqrt( math.pow(A1[i].xcor() - 
                                       A1[j].xcor(), 2) + math.pow(A1[i].ycor() - A1[j].ycor(), 2 ) )
            #dist = math.sqrt( math.pow( math.pow(a.xcor() - b.xcor(), 2) + math.pow(a.ycor() - b.ycor(), 2)) )
            sum_r += dist
    sd = math.sqrt( math.pow(sum_r, 2)/(len(A1)) )
    print(sd)

'''Creates a function that updates moshers coordinates if two moshers collided'''
def beNice(A1,A2): 
    if ((A1.xcor() in range(-290,290) and A2.xcor() in range(-290,290)) and (A1.ycor() in range(-290,290) and A2.ycor() in range(-290,290))):
        A1.dx = random.choice([(-10),10])*A2.mass
        A1.dy = random.choice([(-10),10])*A2.mass
        A2.dx = random.choice([(-10),10])*A1.mass
        A2.dy = random.choice([(-10),10])*A1.mass
        
        A1.setx(A1.xcor() + A1.dx)
        A1.sety(A1.ycor() + A1.dy)
        A2.setx(A2.xcor() + A2.dx)
        A2.sety(A2.ycor() + A2.dy)
        
def struggle(A1,A2): 
    A1.dx = 0
    A1.dy = 0
    A2.dx = 0
    A2.dy = 0
    A1.setx(A1.xcor() + A1.dx)
    A1.sety(A1.ycor() + A1.dy)
    A2.setx(A1.xcor() + 10)
    A2.sety(A1.ycor() + 10)
    
def beWild(A1, A2):
    A1.dx = random.randint(-5,5)
    A1.dy = random.randint(-5,5)
    A1.setx(A1.xcor() + A1.dx)
    A1.sety(A1.ycor() + A1.dy)
    A2.setx(A1.xcor())
    A2.sety(A1.ycor())
    
def excuseMe(A1,A2): 
    A1.dy = random.choice([(-1),1])
    A2.dy = random.choice([(-1),1])
    A1.sety(A1.ycor() + A1.dy)
    A2.sety(A2.ycor() + A2.dy)

'''Creates Animation Loop'''
while True:
    wn.update()
    for blue in blues:
        #assume wall of death, everyone accelerating in x-dir, random motion in y
        blue.dx += 1
        blue.dy = random.randint(-3,3)
        blue.setx(blue.xcor() + blue.dx)
        blue.sety(blue.ycor() + blue.dy)
        print("BLUES:" + str(stDev(blues)))

        #turns when hits wall
        if blue.xcor() < -300:
            blue.dx *= -1
            blue.setx(blue.xcor() + blue.dx)
        if blue.xcor() > 300:
            blue.dx *= -1
            blue.setx(blue.xcor() + blue.dx)
        if blue.ycor() < -300:
            blue.dy *= -1
            
        if blue.ycor() > 300:
            blue.dy *=-1
            
    for green in greens:
    #assume wall of death, everyone accelerating in -x-dir, random motion in y
        green.dx -= 1
        green.dy = random.randint(-3,3)
        green.setx(green.xcor() + green.dx)
        green.sety(green.ycor() + green.dy)
        print("GREENS:" + str(stDev(greens)))
                 
        #turns when hits wall
        if green.xcor() < -300:
            green.dx *= -1
            green.setx(green.xcor() + green.dx)
 
        if green.xcor() > 300:
            green.dx *=-1
            green.setx(green.xcor() + green.dx)
            
            
        if green.ycor() < -300:
            green.dy *= -1
                     
        if green.ycor() > 300:
            green.dy *=-1   
              
    for wild in wilds:
            r = 50
            theta = random.randint(0,30)
            wild.sety(wild.ycor() + r*math.sin(theta))
            wild.setx(wild.xcor() + r*math.cos(theta))
         
               
    'CHECKS FOR IN GROUP COLLISIONS'   
    for blue in blues:
        i = blues.index(blue)
        for blue in blues[0:i]:
            j = blues.index(blue)
            if i != j and isCollision(blue, blue):
                blu_col += 1
                excuseMe(blue, blue)
                #print("blues = " + str(blu_col)) 
                 
    for green in greens:
        i = greens.index(green)
        for green in greens[0:i]:
            j = greens.index(green)
            gn_col += 1
            if i != j and isCollision(green, green):
                excuseMe(green, green)
                #print("greens = " + str(gn_col))
    
    '''CHECKS FOR COLLISIONS BETWEEN GROUPS'''
    for green in greens: 
        for blue in blues:
            if isCollision(blue, green):
                if blue.xcor() in range(-50,50):
                    struggle(blue,green)
                    print("collisions = " + str(col))
                if (blue.xcor() in range(-290,-50)) or (blue.xcor() in range(50,290)):
                    beNice(blue,green)
                    col += 1
                    print("collisions = " + str(col))
                    
    '''CHECKS FOR COLLISIONS BETWEEN WILDS'''
    for wild in wilds: 
        for blue in blues:
            if isCollision(wild, blue):
                beWild(wild, blue)
    '''CHECKS FOR COLLISIONS BETWEEN WILDS'''
    for wild in wilds: 
        for green in greens:
            if isCollision(wild, green):
                beWild(wild, green)

      
                  
turtle.mainloop()

